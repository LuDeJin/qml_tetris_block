#include "block.h"
#include <QTime>
#include <QDebug>

// 1111 0000 0000 0000
#define LINE4 0b1111000000000000
// 0000 1111 0000 0000
#define LINE3 0b0000111100000000
// 0000 0000 1111 0000
#define LINE2 0b0000000011110000
// 0000 0000 0000 1111
#define LINE1 0b0000000000001111

// 0001 0001 0001 0001
#define COLUMN1 0b0001000100010001
// 0010 0010 0010 0010
#define COLUMN2 0b0010001000100010
// 0100 0100 0100 0100
#define COLUMN3 0b0100010001000100
// 1000 1000 1000 1000
#define COLUMN4 0b1000100010001000

// 各个方块二进制表达
const unsigned int BLOCKS[] = {
    0b0001000100010001,               // BLOCK_I      ‭0001 0001 0001 0001‬‬
    0b0000001100100010,               // BLOCK_L_L    0000 ‭0011 0010 0010
    0b0000001100010001,               // BLOCK_L_R    0000 ‭0011 0001 0001
    0b0000000000110011,               // BLOCK_O      0000 0000 ‭0011 0011
    0b0000000000110110,               // BLOCK_Z_L    0000 0000 ‭0011 0110‬
    0b0000000001100011,               // BLOCK_Z_R    0000 0000 ‭0110 0011
    0b0000000000100111                // BLOCK_T      0000 0000 ‭0010 0111
};

Block::Block(const unsigned int &originX)
{
    // 初始化位置
    this->x = originX;
    this->y = 0;
    // 初始化下一个方块
    qsrand(QTime::currentTime().msec()*qrand());
    m_nextBlockData = BLOCKS[qrand()%7];
    // 刷新当前方块，并生成下一个方块
    this->toNext(originX);
}

unsigned int Block::getRevolveBlock()
{
    BlockData newblockData;
    newblockData.data = 0;
    // 简单顺时针旋转90°
    newblockData.b = {m_blockData.b.b12, m_blockData.b.b8 , m_blockData.b.b4, m_blockData.b.b0,
                      m_blockData.b.b13, m_blockData.b.b9 , m_blockData.b.b5, m_blockData.b.b1,
                      m_blockData.b.b14, m_blockData.b.b10, m_blockData.b.b6, m_blockData.b.b2,
                      m_blockData.b.b15, m_blockData.b.b11, m_blockData.b.b7, m_blockData.b.b3};
    // 去掉前面的空格
    while(!(newblockData.data&COLUMN1)) {
        newblockData.data =  newblockData.data >> 1;
    }
    return newblockData.data;
}

unsigned int Block::block() const
{
    return m_blockData.data;
}

void Block::toNext(const unsigned int &originX)
{
    // 重设位置
    this->x = originX;
    this->y = 0;
    // 重设二进制数据
    this->setBlock(m_nextBlockData);
    // 随机生成下一个方块
    qsrand(QTime::currentTime().msec()*qrand());
    m_nextBlockData = BLOCKS[qrand()%7];
}

unsigned int Block::getNext()
{
    return m_nextBlockData;
}

void Block::setBlock(const unsigned int &blockData)
{
    // 修改二进制数据
    m_blockData.data = blockData;
    // 刷新高度和宽度
    m_height = m_blockData.data&LINE4 ? 4 :
               m_blockData.data&LINE3 ? 3 :
               m_blockData.data&LINE2 ? 2 :
                                        1;

    m_width = m_blockData.data&COLUMN4 ? 4 :
              m_blockData.data&COLUMN3 ? 3 :
              m_blockData.data&COLUMN2 ? 2 :
              1;
}

unsigned int Block::height() const
{
    return m_height;
}

unsigned int Block::width() const
{
    return m_width;
}

