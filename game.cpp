#include "game.h"
#include <QDebug>

// 检查游戏是否开始，括号里面为默认return值
#define CHECK_IS_STARTED(x) if(!m_isStarted) return x;

Game::Game(QObject *parent) : QObject(parent)
{
    m_background = new Background();
    m_block = new Block();
    m_score = 0;

    // 坐标变动的变量信号连接
    QObject::connect(this, &Game::block_xChanged, this, &Game::blockPointChanged);
    QObject::connect(this, &Game::block_yChanged, this, &Game::blockPointChanged);
    QObject::connect(this, &Game::blockPointChanged, this, &Game::block_iChanged);
    QObject::connect(this, &Game::blockPointChanged, this, &Game::blockChanged);

    QObject::connect(this, &Game::blockChanged, this, &Game::nextBlockChanged);

    // 设置计时器
    m_timer = new QTimer(this);
    m_fallMsec = 1000;
    QObject::connect(m_timer, &QTimer::timeout, this, &Game::down);
}

Game::~Game()
{
    if(NULL != m_background) {
        delete m_background;
        m_background = NULL;
    }
    if(NULL != m_block) {
        delete m_block;
        m_block = NULL;
    }

    m_timer->deleteLater();
}

void Game::start()
{
    // 创建新方块
    m_block->toNext(m_originX);      // 上一局的下一个方块
    m_block->toNext(m_originX);      // 再修改一次

    // 新背景设置
    m_background->clearAll();
    m_background->setRows(m_rows);
    m_background->setColumns(m_columns);

    // 游戏开始状态设置
    m_isStarted = true;
    m_isGameOver = false;
    emit isStartedChanged();
    emit isGameOveredChanged();
    emit blockChanged();

    // 计时器开始
    m_timer->start(m_fallMsec);
}

void Game::left()
{
    CHECK_IS_STARTED()

    if(m_block->x != 0) {
        // 判断方块是否被背景阻挡
        if(m_background->checkOverlap(m_block->block(), m_block->x-1, m_block->y))
            return;
        // 向左
        --m_block->x;
        emit block_xChanged();
    }
}

void Game::right()
{
    CHECK_IS_STARTED()

    if(m_block->x + m_block->width() != m_columns) {
        // 判断方块是否被背景阻挡
        if(m_background->checkOverlap(m_block->block(), m_block->x+1, m_block->y))
            return;
        // 向右
        ++m_block->x;
        emit block_xChanged();
    }
}

void Game::up()
{
    CHECK_IS_STARTED()

    if(m_columns >= (m_block->x + m_block->height())) {
        // 判断方块是否被背景阻挡
        unsigned int revolveBlock = m_block->getRevolveBlock();
        if(m_background->checkOverlap(revolveBlock, m_block->x, m_block->y))
            return;
        // 旋转
        m_block->setBlock(revolveBlock);
        emit blockChanged();
    }
}

bool Game::down()
{
    CHECK_IS_STARTED(false)

    if(m_background->checkOverlap(m_block->block(), m_block->x, m_block->y+1)       // 接触到背景
            || m_block->height() + m_block->y >= m_rows) {                           // 到达底部
        // 融合
        m_background->merge(m_block);
        // 消行
        this->setScore(m_score + m_background->elimLine());
        // 修改方块
        m_block->toNext(m_originX);
        emit blockChanged();
        // 判断游戏结束
        if(m_background->checkOverlap(m_block->block(), m_block->x, m_block->y)) {
            doGameOver();
        }
        return false;
    } else {
        // 方块下落
        ++m_block->y;
        emit block_yChanged();
        return true;
    }

}

void Game::fallsDown(const int &fallMsec)
{
    while(down());
}

int Game::fallMsec() const
{
    return m_fallMsec;
}

void Game::setFallMsec(int fallMsec)
{
    if(m_fallMsec != fallMsec) {
        m_fallMsec = fallMsec;
        m_timer->setInterval(m_fallMsec);
        emit fallMsecChanged();
    }
}

void Game::doGameOver()
{
    m_isStarted = false;
    emit isStartedChanged();
    m_isGameOver = true;
    emit isGameOveredChanged();
}

void Game::setScore(unsigned int score)
{
    if(m_score != score) {
        m_score = score;
        emit scoreChanged();
    }
}

unsigned int Game::score() const
{
    return m_score;
}

unsigned int Game::originX() const
{
    return m_originX;
}

void Game::setOriginX(unsigned int originX)
{
    m_originX = originX;
}

bool Game::isGameOver() const
{
    return m_isGameOver;
}

unsigned int Game::rows() const
{
    return m_rows;
}

void Game::setRows(const unsigned int &rows)
{
    m_rows = rows;
}

unsigned int Game::columns() const
{
    return m_columns;
}

void Game::setColumns(const unsigned int &columns)
{
    m_columns = columns;
}

unsigned int Game::block() const
{
    CHECK_IS_STARTED(0)
    return m_block->block();
}

unsigned int Game::nextBlock() const
{
    CHECK_IS_STARTED(0)
    return m_block->getNext();
}

unsigned int Game::block_i() const
{
    CHECK_IS_STARTED(0)
    return m_block->x + m_block->y*m_columns;
}

unsigned int Game::block_y() const
{
    CHECK_IS_STARTED(0)
    return m_block->y;
}

unsigned int Game::block_x() const
{
    CHECK_IS_STARTED(0)
    return m_block->x;
}

QVector<int> Game::background() const
{
    return m_background->getData();
}

bool Game::isStarted() const
{
    return m_isStarted;
}

