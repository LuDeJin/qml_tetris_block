#ifndef BACKGROUND_H
#define BACKGROUND_H
#include <QList>
#include "block.h"

class Background
{
public:
    Background();
    Background(const unsigned int &rows, const unsigned int &columns);

    void merge(const Block *block);                         // 将方块添加入背景
    unsigned int elimLine();                                // 消行，并返回行数
    bool checkOverlap(const unsigned int& blockData,
                      const unsigned int &block_x,
                      const unsigned int &block_y);         // 检查方块是否重叠
    void clearAll();                                        // 清空所有方格

    QVector<int> getData() const;
    unsigned int getRows() const;
    void setRows(unsigned int rows);
    unsigned int getColumns() const;
    void setColumns(unsigned int columns);

private:
    bool get(const unsigned int &index);                    // 返回指定位置的一格方格
    void add(const unsigned int &index);                    // 在指定位置插入一个一格方块
    void clear(const unsigned int &index);                  // 删除指定位置上的一格方块
    void clearLine(const unsigned int &row);                // 删除一整行
    void initData();                                        // 初始化背景的二进制数据

    inline unsigned int block_i(const unsigned int &block_x,
                                const unsigned int &block_y) const;     // 以坐标计算该方格的序号

    QVector<int> m_data;         // 背景的二进制数据,每个int可以表示一个32个格子的一整块,从左上的方格为第一格

    unsigned int m_rows;         // 行数，从0开始计数
    unsigned int m_columns;      // 列数，从0开始计数
};

#endif // BACKGROUND_H
