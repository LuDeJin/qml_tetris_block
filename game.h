#ifndef GAME_H
#define GAME_H

#include "block.h"
#include "background.h"

#include <QObject>
#include <QList>
#include <QVector>
#include <QTimer>

class Game : public QObject
{
    Q_OBJECT
    // 可写
    Q_PROPERTY(unsigned int rows READ rows WRITE setRows NOTIFY rowsChanged)
    Q_PROPERTY(unsigned int columns READ columns WRITE setColumns NOTIFY columnsChanged)
    Q_PROPERTY(unsigned int originX READ originX WRITE setOriginX NOTIFY originXChanged)
    Q_PROPERTY(int fallMsec READ fallMsec WRITE setFallMsec NOTIFY fallMsecChanged)

    // 只读
    Q_PROPERTY(unsigned int score READ score NOTIFY scoreChanged)
    Q_PROPERTY(unsigned int block READ block NOTIFY blockChanged)
    Q_PROPERTY(unsigned int nextBlock READ nextBlock NOTIFY nextBlockChanged)
    Q_PROPERTY(QVector<int> background READ background NOTIFY backgroundChanged)
    Q_PROPERTY(unsigned int block_x READ block_x NOTIFY block_xChanged)
    Q_PROPERTY(unsigned int block_y READ block_y NOTIFY block_yChanged)
    Q_PROPERTY(unsigned int block_i READ block_i NOTIFY block_iChanged)
    Q_PROPERTY(bool isStarted READ isStarted NOTIFY isStartedChanged)
    Q_PROPERTY(bool isGameOver READ isGameOver NOTIFY isGameOveredChanged)
public:
    explicit Game(QObject *parent = nullptr);
    ~Game();

    // 启动操作
    Q_INVOKABLE void start();
    void pause();
    void restart();

    // 游戏控制
    Q_INVOKABLE void left();
    Q_INVOKABLE void right();
    Q_INVOKABLE void up();
    Q_INVOKABLE bool down();
    Q_INVOKABLE void fallsDown(const int &fallMsec=0);       // 自动垂直下落，下到底

    unsigned int rows() const;
    void setRows(const unsigned int &rows);

    unsigned int columns() const;
    void setColumns(const unsigned int &columns);

    unsigned int block() const;
    unsigned int nextBlock() const;

    QVector<int> background() const;

    unsigned int block_x() const;
    unsigned int block_y() const;
    unsigned int block_i() const;   // 方块第一格在背景中的下标位置，从i=0开始计算

    bool isStarted() const;

    int fallMsec() const;
    void setFallMsec(int fallMsec);

    bool isGameOver() const;

    unsigned int originX() const;
    void setOriginX(unsigned int originX);

    unsigned int score() const;
    void setScore(unsigned int score);

signals:
    void rowsChanged();
    void columnsChanged();
    void originXChanged();
    void fallMsecChanged();

    void scoreChanged();
    void blockChanged();
    void nextBlockChanged();
    void backgroundChanged();
    void block_xChanged();
    void block_yChanged();
    void block_iChanged();
    void blockPointChanged();
    void isStartedChanged();
    void isGameOveredChanged();

public slots:
private:
    void doGameOver();          // 游戏结束时的动作

    unsigned int m_rows;        // 行数
    unsigned int m_columns;     // 列数

    QTimer *m_timer;              // 下降计时器
    int m_fallMsec;             // 下降时间间隔，默认为1000ms

    unsigned int m_originX;     // 生成方块的x位置
    unsigned int m_score;       // 分数

    Background *m_background;
    Block *m_block;

    bool m_isStarted;           // 是否已经开始
    bool m_isGameOver;          // 游戏是否已经结束了
};



#endif // GAME_H
