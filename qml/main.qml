import QtQuick 2.10
import QtQuick.Window 2.10

Window {
    visible: true
    width: loader.width
    height: loader.height
    title: qsTr("qml俄罗斯方块")


    Loader {
        id: loader
        //asynchronous: true
        source: "MainView.qml"
        focus: true
    }
}
