import QtQuick 2.10
import QtQuick.Controls 2.3
import com.ozre.Game 1.0
FocusScope {
    focus: true
    width: 2 + game.columns*game.blockSize + 2 + game.blockSize*5 + 2
    height: 2 + game.rows*game.blockSize + 2

    // 游戏逻辑实现
    Game {
        id: game
        property int blockSize: 20
        property int defaultFallMsec: 1000          // 默认的下降时间
        property int defaultQuickFallMsec: 30       // 快速下降的时间
        rows: 30
        columns: 20
        originX: 4
        fallMsec: defaultFallMsec

        onBlockChanged: {
            canvas.requestPaint()
        }

        onNextBlockChanged: {
            canvasNextBlock.requestPaint()
        }
    }

    // 键盘操作监听器
    Item {
        anchors.fill: parent

        focus: true
        Keys.enabled: true
        Keys.onPressed: {
            switch(event.key) {
            case Qt.Key_Shift:
                game.fallMsec = game.defaultQuickFallMsec
                break
            case Qt.Key_Up:
                game.up()
                break
            case Qt.Key_Left:
                game.left()
                break
            case Qt.Key_Right:
                game.right()
                break
            case Qt.Key_Down:
                game.down()
                break
            case Qt.Key_Space:
                game.fallsDown()
                break
            default:
                break
            }
        }

        Keys.onReleased: {
            switch(event.key) {
            case Qt.Key_Shift:
                game.fallMsec = game.defaultFallMsec
                break
            }
        }

        // 始终获得焦点
        onFocusChanged: {
            focus = true
        }

    }

    // 开始按钮
    Button {
        id: btnStart
        text: "开始"
        anchors.left: rectMain.right
        anchors.top: txtScore.bottom
        anchors.margins: 2
        onClicked: {
            game.start()
        }
    }

    // 显示分数
    Text {
        id: txtScore
        text: "分数：" + game.score
        anchors.left: rectMain.right
        anchors.top: rectNextBlock.bottom
        anchors.margins: 2
    }


    // 显示下一个方块的面板
    Rectangle {
        id: rectNextBlock
        border.width: 1
        width: game.blockSize*5
        height: game.blockSize*5
        anchors.top: parent.top
        anchors.topMargin: 2
        anchors.left: rectMain.right
        anchors.leftMargin: 2

        Canvas {
            id: canvasNextBlock
            anchors.fill: parent
            onPaint: {
                var ctx = getContext("2d");

                // 擦除
                ctx.clearRect(game.blockSize,game.blockSize,game.blockSize*4,game.blockSize*4)

                // 绘制方块
                var i=0
                var x=game.blockSize
                var y=game.blockSize
                for(var iy=0; iy<4; y+=game.blockSize, ++iy) {
                    for(var ix=0; ix<4; x+=game.blockSize, ++ix) {
                        if((game.nextBlock & (Math.pow(2, i))) !== 0) {
                            ctx.fillRect(x, y, game.blockSize, game.blockSize);
                        }
                        ++i;
                    }
                    x=game.blockSize

                }
            }
        }
    }

    // 俄罗斯方块游戏主界面
    Rectangle {
        id: rectMain
        width: game.blockSize*game.columns
        height: game.blockSize*game.rows
        border.width: 1
        anchors.left: parent.left
        anchors.leftMargin: 2
        anchors.top: parent.top
        anchors.topMargin: 2

        Canvas {
            id: canvas
            anchors.fill: parent
            onPaint:
            {
                var ctx = getContext("2d");
                ctx.strokeRect(0,0,game.blockSize*game.columns,game.blockSize*game.rows)

                // 清除上一层block

                for(var i=0,y=0; i<game.rows; ++i,y+=game.blockSize) {
                    for(var j=0,x=0; j<game.columns; ++j,x+=game.blockSize) {
                        ctx.clearRect(x,y,game.blockSize,game.blockSize)
                        //ctx.strokeRect(x,y,game.blockSize,game.blockSize)
                    }
                }

                // 绘制背景
                i=0
                x=0
                y=0
                var blocks = []
                blocks = game.background
                var indexBlock = 0
                if(blocks.length > 0) {

                    for(var row = game.rows; row > 0; --row) {
                        for(var column = game.columns; column > 0; --column) {
                            //console.log(blocks[indexBlock])
                            if((blocks[indexBlock] & (Math.pow(2, i-indexBlock*32))) !== 0) {
                                ctx.fillRect(x, y, game.blockSize, game.blockSize);
                            }
                            ++i
                            if(i%32 == 0) {
                                ++indexBlock
                            }
                            x += game.blockSize
                        }
                        x = 0
                        y += game.blockSize
                    }
                }

                // 绘制方块
                i=0
                x=game.block_x*game.blockSize
                y=game.block_y*game.blockSize
                for(var iy=0; iy<4; y+=game.blockSize, ++iy) {
                    for(var ix=0; ix<4; x+=game.blockSize, ++ix) {
                        if((game.block & (Math.pow(2, i))) !== 0) {
                            ctx.fillRect(x, y, game.blockSize, game.blockSize);
                        }
                        ++i;
                    }
                    x=game.block_x*game.blockSize
                }
            }
        }

        // 游戏结束显示板
        Rectangle {
            id: rectGameOverShow
            anchors.fill: parent
            color: "#00000000"
            visible: game.isGameOver
            Text {
                anchors.centerIn: parent
                color: "red"
                text: qsTr("游戏结束！")
                font.pointSize: 16
            }
        }

    }


}
