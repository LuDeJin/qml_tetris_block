#ifndef BLOCK_H
#define BLOCK_H

// 一个整型可以表示32二进制位，现在只操作前面的16个二进制位
union BlockData{
    unsigned int data;
    struct{
        unsigned char b0:1;
        unsigned char b1:1;
        unsigned char b2:1;
        unsigned char b3:1;
        unsigned char b4:1;
        unsigned char b5:1;
        unsigned char b6:1;
        unsigned char b7:1;
        unsigned char b8:1;
        unsigned char b9:1;
        unsigned char b10:1;
        unsigned char b11:1;
        unsigned char b12:1;
        unsigned char b13:1;
        unsigned char b14:1;
        unsigned char b15:1;
    } b;
};

class Block
{
public:
    /*
    enum BLOCK_TYPE {
        BLOCK_I,            // 长条
        BLOCK_L_L,          // L左
        BLOCK_L_R,          // L右
        BLOCK_O,            // 四方格
        BLOCK_Z_L,          // Z左
        BLOCK_Z_R,          // Z右
        BLOCK_T             // T
    };
    */

    // 随机生成一个方块
    Block(const unsigned int &originX=0);


    unsigned int getRevolveBlock();                     // 返回一个转动方块，不会改变自身
    unsigned int block() const;                         // 返回本身的数据

    void toNext(const unsigned int &originX=0);         // 设置当前方块为下一个，并随机生成下一个方块
    unsigned int getNext();                             // 返回下一个方块二进制数据

    void setBlock(const unsigned int& blockData);       // 重新设置方块二进制数据

    unsigned int height() const;
    unsigned int width() const;

    unsigned int x;     // 方块第一格的x位置, 最左边为x=0
    unsigned int y;     // 方块第一格的y位置, 最上边为y=0

private:
    BlockData m_blockData;          // 方块数据二进制表示
    unsigned int m_nextBlockData;   // 下一个方块数据的二进制

    unsigned int m_height;          // 高度
    unsigned int m_width;           // 宽度

};

#endif // BLOCK_H
