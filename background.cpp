#include "background.h"
#include <QDebug>

#define INT_BITS_LENGTH 32

Background::Background()
{
    m_rows = 0;
    m_columns = 0;
    //m_data.clear();
}

Background::Background(const unsigned int &rows, const unsigned int&columns)
{
    m_rows = rows;
    m_columns = columns;
    initData();
}

void Background::merge(const Block *block)
{
    // 添加方块进入背景
    int index = block->x + block->y*m_columns;        // 方格序号
    int i = 0;                                        // 方块的二进制位

    for(int block_row=0; block_row<4; ++block_row) {
        for(int block_columns=0; block_columns<4; ++block_columns) {

            if(block->block() & (int)pow(2, i)) {
                add(index);
            }

            ++index;
            ++i;
        }
        index+=m_columns-4;
    }
}

unsigned int Background::elimLine()
{
    unsigned int elimSum = 0;       // 消掉行的数目
    unsigned int beginOfLineIndex = 0;  // 每一行的行首位置

    // 遍历每一行
    for(unsigned int row=0; row<m_rows; ++row) {
        bool isLine = true;

        // 遍历当前行，发现有空格就跳出循环
        unsigned int index = beginOfLineIndex;
        for(unsigned int column=0; column<m_columns; ++column) {
            if(!get(index)) {
                isLine = false;
                break;
            }
            ++index;
        }

        // 检查循环是否被break,如果没有，则当前行格子全为非空
        if(isLine) {
            clearLine(row);
            ++elimSum;
        }

        beginOfLineIndex += m_columns;
    }
    return elimSum;
}


bool Background::checkOverlap(const unsigned int &blockData, const unsigned int &block_x, const unsigned int &block_y)
{
    int index = block_i(block_x, block_y);          // 对应方块上的每个方格在背景上的序号
    int i = 0;                                      // 方块上的二进制位

    // 遍历方块的每一个方格
    for(int block_row=0; block_row<4; ++block_row) {
        for(int block_columns=0; block_columns<4; ++block_columns) {

            if(blockData & (int)pow(2, i)) {
                if(get(index)) {        // 发现该方块的一个方格与背景重合
                    return true;
                }
            }

            ++index;
            ++i;
        }
        index+=m_columns-4;
    }
    // 未发现重合的方格
    return false;
}

void Background::clearAll()
{
    if(m_rows != 0 && m_columns != 0) {
        initData();
    } else {
        m_data.clear();
    }
}

bool Background::get(const unsigned int &index)
{
    int blockIndex = index/INT_BITS_LENGTH;
    if(m_data.length()-1 < blockIndex) {
        qDebug() << "get blockIndex error, blockIndex:" << blockIndex;
        return false;
    }
    return m_data[blockIndex]&(int)pow(2, index%INT_BITS_LENGTH);
}

void Background::add(const unsigned int &index)
{
    int blockIndex = index/INT_BITS_LENGTH;
    if(m_data.length()-1 < blockIndex) {
        qDebug() << "add blockIndex error, blockIndex:" << blockIndex;
        return;
    }
    m_data[blockIndex] |= (int)pow(2, index%INT_BITS_LENGTH);
}

void Background::clear(const unsigned int &index)
{
    int blockIndex = index/INT_BITS_LENGTH;
    if(m_data.length()-1 < blockIndex) {
        qDebug() << "clear blockIndex error, blockIndex:" << blockIndex;
        return;
    }
    m_data[blockIndex] &= (~(int)pow(2, index%INT_BITS_LENGTH));
}

void Background::clearLine(const unsigned int &row)
{
    if(row!=0)
    {
        unsigned int lineBlockIndex = row*m_columns + m_columns-1;      // 遍历当前行的每个位置
        unsigned int lastLineBlockIndex = lineBlockIndex - m_columns;   // 遍历当前行的上一行的每个位置

        // 将上一行复制到当前行
        for(;lastLineBlockIndex>0; --lineBlockIndex, --lastLineBlockIndex) {
            if(get(lastLineBlockIndex)) {
                add(lineBlockIndex);
            } else {
                clear(lineBlockIndex);
            }
        }
    }

    // 清空第一行
    for(unsigned int i = m_columns-1;i>0; --i) {
        clear(i);
    }
}

void Background::initData()
{
    // 预计分块数
    unsigned int blockSum = (m_rows*m_columns)/INT_BITS_LENGTH + (m_rows*m_columns)%INT_BITS_LENGTH%1 + 1;
    m_data.clear();
    // 逐个添加块
    while(blockSum--) {
        m_data.append(0);
    }
}

unsigned int Background::block_i(const unsigned int &block_x, const unsigned int &block_y) const
{
    return block_x + block_y*m_columns;
}

QVector<int> Background::getData() const
{
    return m_data;
}

unsigned int Background::getRows() const
{
    return m_rows;
}

void Background::setRows(unsigned int rows)
{
    if(m_rows != rows) {
        m_rows = rows;
        if(m_columns !=0) {
            initData();
        }
    }
}

unsigned int Background::getColumns() const
{
    return m_columns;
}

void Background::setColumns(unsigned int columns)
{
    if(m_columns != columns) {
        m_columns = columns;
        if(m_rows !=0) {
            initData();
        }
    }
}

